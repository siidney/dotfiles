-- $CONFIG/nvim/lua/plugins/colorscheme.lua

return {
    -- the colour scheme should be available when staring NeoVim
    {
        "rebelot/kanagawa.nvim",
        lazy = false, -- make sure we load this during startup if it is your main colourscheme
        priority = 1000, -- make sure to load this before all the other start plugins
        config = function()
            -- load the colourscheme here
            vim.cmd([[colorscheme kanagawa-dragon]])
        end,
    },
}
