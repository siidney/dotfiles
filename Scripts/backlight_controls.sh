#!/bin/sh

#   Filename:       backlight_controls.sh
#   Created:        03/04/19 11:32:48
#   Revision:
#   Version:        1.00
#   Author:         Siidney Watson
#   Description:
#
#   Simple bash script which changes the backlight brightness by +- $STEP.
#   Written as I couldn't get any other method to work with keyboard shortcuts
#   on this laptop.
#
#   Requires user to have permission to write to /sys/class/backlight/video/brightness
#

STEP=20
backlight_dir="/sys/class/backlight/amdgpu_bl1"
max=`cat ${backlight_dir}/max_brightness`
current=`cat ${backlight_dir}/brightness`

# Set the Brightness
set_brightness() {
    current=$1

    if [ $current -lt 1 ]; then
        current=1
    elif [ $current -gt $max ]; then
        current=$max
    fi

    echo $current > ${backlight_dir}/brightness
}

# Increment/Decrement the current Brightness
case "$1" in
    up)
        let "current+= $STEP"
        set_brightness $current
        ;;
    down)
        let "current-=$STEP"
        set_brightness $current
        ;;
    max)
        let "current=$max"
        set_brightness $current
        ;;
    *)
        exit 1
esac
