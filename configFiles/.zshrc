# The following lines were added by compinstall
#
zstyle ':completion:*' completer _expand _complete _ignored
zstyle :compinstall filename '/home/siid/.zshrc'

export EDITOR=/usr/bin/vim
export VISUAL=/usr/bin/vim
export BROWSER=/usr/bin/luakit
export PANEL_FIFO=/tmp/panel-fifo
export PATH=$PATH:/$HOME/.config/bspwm/panel

autoload -Uz compinit promptinit
compinit
promptinit

# Set default prompt
prompt redhat

# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
unsetopt beep
bindkey -v

# End of lines configured by zsh-newuser-install
#
# Enable history search
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "$key[Up]"   ]] && bindkey -- "$key[Up]"   up-line-or-beginning-search
[[ -n "$key[Down]" ]] && bindkey -- "$key[Down]" down-line-or-beginning-search

# Automatically find new executables in $PATH
zstyle ':completion:*' rehash true

# ALIAS
alias quickComp="g++ -o main *.cpp -g -Wall -pipe -O3 --std=c++14 -lm"
alias quickCompc="gcc -o main *.c -g -Wall -pipe -O3 --std=c11 -lm"
