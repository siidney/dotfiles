vim.g.mapleader = ','

local key_mapper = function(mode, key, command)
	vim.api.nvim_set_keymap(mode, key, command, {noremap = true, silent = true})
end

-- disable arrow keys
key_mapper('n', '<up>', '<nop>')
key_mapper('n', '<down>', '<nop>')
key_mapper('n', '<left>', '<nop>')
key_mapper('n', '<right>', '<nop>')
key_mapper('i', '<up>', '<nop>')
key_mapper('i', '<down>', '<nop>')
key_mapper('i', '<left>', '<nop>')
key_mapper('i', '<right>', '<nop>')
key_mapper('n', '<space>', '<nop>')

-- jk to exit insert mode
key_mapper('i', 'jk', '<ESC>')

-- enter command line mode
key_mapper('n', '<Space>', ':')

-- reload config file
key_mapper('n','<leader>sv', ':luafile $MYVIMRC<CR>')

-- clear search
key_mapper('n', '<leader><space>', ':noh<CR>')

-- save file
key_mapper('n', '<leader>s', ':w<CR>')
key_mapper('i', '<leader>s', '<C-c>:w<CR>')

-- list open buffers and prompt to switch
key_mapper('n', '<F5>', ':ls<CR>:b<space>')

-- open new empty buffer
key_mapper('n', '<leader>b', ':enew<CR>')

-- close current buffer
key_mapper('n', '<leader>d', ':bd<CR>')

-- buffer switching
key_mapper('n', '<leader>l', ':bnext<CR>')
key_mapper('n', '<leader>h', ':bprev<CR>')
