-- $CONFIG/nvim/lua/plugins/mason.lua

return {
    "williamboman/mason.nvim",
    dependencies = {
        "williamboman/mason-lspconfig.nvim",
        "WhoIsSethDaniel/mason-tool-installer.nvim",
    },
    config = function()
        require("mason").setup()

        require("mason-lspconfig").setup({
            automatic_installation = true,
            ensure_installed = {
                "clangd",
                "pyright",
                "marksman",
            },
        })

        require("mason-tool-installer").setup({
            ensure_installed = {
                "cpplint",
                "clangd",
                "pyright",
                "marksman",
            },
        })
    end,
}
