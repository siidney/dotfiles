# Dotfiles

Various system config files and 'useful' Scripts.

## Folders
> - [configFiles](https://gitlab.com/siidney/dotfiles/tree/master/configFiles) - System config files
> - [nvim](https://gitlab.com/siidney/dotfiles/tree/master/nvim) - NVim config files
> - [References](https://gitlab.com/siidney/dotfiles/tree/master/References) - Various Reference material
> - [Scripts](https://gitlab.com/siidney/dotfiles/tree/master/Scripts) - Various useful (to me) system Scripts
> - [wm](https://gitlab.com/siidney/dotfiles/tree/master/wm) - bspwm config files

## Scripts
> - [amazon-music.sh](https://gitlab.com/siidney/dotfiles/blob/master/Scripts/amazon-music.sh) - Opens Amazon Music in browser session.
> - [backlight_controls.sh](https://gitlab.com/siidney/dotfiles/blob/master/Scripts/backlight_controls.sh) - Backlight control script.
> - [battery-notify](https://gitlab.com/siidney/dotfiles/blob/master/Scripts/batter-notify) - Notifies battery level.
> - [oneliners](https://gitlab.com/siidney/dotfiles/blob/master/Scripts/oneliners) - Useful Shell one liners.
> - [rtorrent-magnet](https://gitlab.com/siidney/dotfiles/blob/master/Scripts/rtorrent-magnet) - Opens magnet files from browser.
> - [runorraise.sh](https://gitlab.com/siidney/dotfiles/blob/master/Scripts/runorraise.sh) - Run or raise applications
> - [slideShow.sh](https://gitlab.com/siidney/dotfiles/blob/master/Scripts/slideShow.sh) - Bash slideshow script using feh
> - [tmux-startup](https://gitlab.com/siidney/dotfiles/blob/master/Scripts/tmux-startup) - Tmux startup script.
> - [toggle-touchpad.sh](https://gitlab.com/siidney/dotfiles/blob/master/Scripts/toggle-touchpad.sh) - Toggles the touchpad.
