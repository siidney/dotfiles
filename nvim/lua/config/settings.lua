--[ GENERAL SETTINGS ]--

-- set shell
vim.o.shell = "/bin/zsh"

-- enable system clipboard access
vim.o.clipboard = "unnamedplus"

-- encoding
vim.o.encoding = "utf-8"
vim.o.fileencoding = "utf-8"

-- line endings
vim.o.fileformat = "unix"
vim.o.fileformats = "unix,dos"

-- automatically change working dir to current file
vim.o.autochdir = true

-- reload files edited outside of nvim
vim.o.autoread = true

-- make backspace work
vim.o.backspace = "indent,eol,start"

-- show line numbers
vim.o.number = true

-- create undo file on close
vim.o.undofile = true

-- completion
vim.o.completeopt = "menu,menuone,noselect"

vim.o.termguicolors = true

--[ INDENTATION ]--

-- set indent based on line above
vim.o.autoindent = true

-- insert spaces whenever tab is pressed
vim.o.expandtab = true

-- when reading, tabs are 4 spaces
vim.o.shiftwidth = 4

-- insert mode tabs are 4 spaces
vim.o.softtabstop = 4

vim.o.smarttab = true

--[ CLEAR WHITESPACE ]--
-- highlight whitespace and remove trailing whitespace
vim.api.nvim_command([[
    highlight ExtraWhitespace ctermbg=red guibg=red
    match ExtraWhitespace /\s\+$/
    au BufWinEnter * match ExtraWhitespace /\s\+$/
    au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    au InsertLeave * match ExtraWhitespcae /\s\+$/
    au BufWinLeave * call clearmatches()
]])

--[ DISABLE AUTOCOMMENT ]--
vim.api.nvim_create_autocmd("BufEnter", {command=[[set formatoptions-=cro]]})
